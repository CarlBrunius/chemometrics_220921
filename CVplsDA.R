#' Single crossvalidation for PLS-DA
#' 
#' This procedure performs PLS-DA with single crossvalidation, 
#' Reports predictions both for fit-predict (CV used for nComp only) as well as hold out (less biased) predictions
#'
#' @param X Predictor data (samples in rows, variables in columns)
#' @param Y Target vector (coerced into factor)
#' @param nFold Number of folds in crossvalidation
#' @param nCompMax Max limit for number of components (defaults to 5)
#'
#' @return
#' @export
#'
#' @examples
CVplsDA <- function(X, Y, nFold = 5, nCompMax = 5) {
  
  # Sanity checks
  if(ncol(X)<2) stop('You need at least 2 variables in your X matrix')
  if(min(table(Y)) < nFold) stop('nFold should be ≤ the smallest class')
  
  # Sample indexing
  nSamp <- length(Y)
  sampIndex <- 1:nSamp
  
  # Force Y as factor and extract levels
  Y <- as.factor(Y)
  Ylev <- levels(Y)
  nClass <- length(Ylev)
  
  # Convert Y to one-hot
  Ymat <- matrix(0, 
                 nrow = nSamp,
                 ncol = nClass)
  for (y in 1:nClass) {
    Ymat[Y == Ylev[y], y] = 1
  }
  
  # Extract groups corresponding to the levels
  groupID <- list()
  for (g in 1:nClass) {
    groupID[[g]] <- sampIndex[Y == Ylev[g]]
  }
  
  # Divide data into segments per group
  groupTest <- list()
  for (gT in 1:nClass) {
    groupTest[[gT]] <- vectSamp(groupID[[gT]], n = nFold)
  }
  
  # Recombine group segments into overall segments
  allTest <- groupTest[[1]]
  for (gT in 2:nClass) {
    allTest <- allTest[order(sapply(allTest, length))]
    for (aT in 1:nFold) {
      allTest[[aT]] <- sort(c(allTest[[aT]], groupTest[[gT]][[aT]]))
    }
  }
  
  # perform crossvalidation
  plsMod <- list()
  plsPred <- array(dim = c(length(Y), ncol(Ymat), nCompMax))
  for (fold in 1:nFold) {
    # Extract holdout test data
    testIndex <- allTest[[fold]]
    Ymat_test <- Ymat[testIndex,]
    X_test <- X[testIndex,]
    
    # Extract training data
    trainIndex <- sampIndex[!sampIndex %in% testIndex]
    Ymat_train <- Ymat[trainIndex,]
    X_train <- X[trainIndex,]
    
    # Train inner model
    plsMod[[fold]] <- plsr(Ymat_train ~ X_train, ncomp = nCompMax)
    plsPred[testIndex,,] <- predict(plsMod[[fold]],X_test)
  }
  
  # Extract predictions and convert back to factor levels
  yPred <- apply(plsPred, c(1,3), which.max)
  yPredFact <- apply(yPred, 2, function(x) factor(Ylev[x]))
  
  # Calculate classification rate at resp nComp
  CR <- apply(yPredFact, 2, function(x) sum(x == Y)) / length(Y)
  
  # Extract optimum nComp
  nCompBest <- which.max(CR)
  
  # Extract CV predictions
  yPredCV <- factor(yPredFact[, nCompBest])
  missCV <- sum(Y != yPredCV)
  tableCV <- table(actual = Y, predicted = yPredCV)
  
  # train consensus model and get fit-predicts
  plsMod <- plsr(Ymat~X, ncomp = nCompBest)
  plsPred <- predict(plsMod,X)[,,nCompBest]
  yPred <- apply(plsPred, 1, which.max)
  yPredFact <- factor(Ylev[yPred])
  scores <- plsMod$scores
  loadings <- plsMod$loadings
  table <- table(actual = Y, predicted = yPredFact)
  misses <- sum(Y != yPredFact)
  return(list(actual = Y,
              predict_CV = yPredCV,
              miss_CV = missCV,
              table_CV = tableCV,
              nComp = nCompBest,
              CR = CR,
              predict_fit = yPredFact,
              miss_fit = misses,
              table_fit = table,
              scores = scores,
              loadings = loadings))
}

