#######################################################################
# Multivariate modelling with Unbiased Variable selection in R - MUVR 

# Repository
# https://gitlab.com/CarlBrunius/MUVR

# Original publication
# Lin et al 2019. Variable selection and validation in multivariate modelling. 
# Bioinformatics 35(6): 972-980. DOI: 10.1093/bioinformatics/bty710




###################################################
# Regression example using the "freelive" data

# Call in relevant libraries
if(!require(remotes)) install.packages('remotes')
if(!require(MUVR)) remotes::install_gitlab('CarlBrunius/MUVR')
library(doParallel)     # Parallel processing
library(MUVR)           # Multivariate modelling

# Clean slate 
rm(list = ls())

# Load the "freelive" data from the MUVR package
#   This data represents a free-living population with repeated sampling
#   Participants recorded food consumption and donated urine samples for metabolomics
data("freelive") 
#   YR     Consumption of whole grain
#   XRVIP  Urine metabolomics data
#   IDR    Psedonymized study ID of participant (for repeated sampling)

# Set method parameters
nCore <- detectCores() - 1   # Number of processor threads to use (15 on my computer)
nRep <- nCore                # Number of MUVR repetitions (normally in multiples of nCore; QnD nCore; Production 20-60)
nOuter <- 5                  # Number of outer cross-validation segments (normally 5-8, depending on sample size)
varRatio <- 0.65             # Proportion of variables kept per iteration (QnD 0.6-0.7; Production 0.75-0.85)
method <- 'PLS'              # Selected core modelling algorithm (normally starting with RF)

# Set up parallel processing
cl <- makeCluster(nCore)   
registerDoParallel(cl)

# Perform modelling (0.26 mins)
RyeModel <- MUVR(X = XRVIP, 
                 Y = YR, 
                 ID = IDR, 
                 scale = TRUE, 
                 nRep = nRep, 
                 nOuter = nOuter, 
                 varRatio = varRatio, 
                 method = method)

# Stop parallel processing
stopCluster(cl)

# Validation and prediction plots
par(mfrow = c(2, 1), 
    mar = c(4, 4, 2, 0) + .5)
plotVAL(RyeModel)
title(main='PLS Regression Validation Plot')
plotMV(RyeModel, model='min')
title(main='PLS Regression Prediction Plot')

# Examine model performance and output
RyeModel$fitMetric                # Look at fitness metrics for min, mid and max models
RyeModel$nComp                    # Number of components for min, mid and max models; for PLS only
RyeModel$nVar                     # Number of variables for min, mid and max models
cbind(YR, RyeModel$yPred)[1:20,]  # Actual exposures side-by-side with min, mid and max predictions

# Selected variables
getVIRank(RyeModel, model='min')   # Extract most informative variables: Lower rank is better
par(mar=c(4,11,2,0)+.5)
plotVIRank(RyeModel, model='min')
title(main='Selected variables')
